<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Triangle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;


class TriangleController extends Controller
{
    /**
     * Lists all triangle entities.
     *
     * @Route("/", name="triangle_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $triangles = $em->getRepository('AppBundle:Triangle')->findAll();

        return $this->render('triangle/index.html.twig', array(
            'triangles' => $triangles,
        ));
    }

    /**
     * Creates a new triangle entity.
     *
     * @Route("/new", name="triangle_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $triangle = new Triangle();
        $form = $this->createForm('AppBundle\Form\TriangleType', $triangle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($triangle);
            $em->flush($triangle);

            return $this->redirectToRoute('triangle_show', array('id' => $triangle->getId()));
        }

        return $this->render('triangle/new.html.twig', array(
            'triangle' => $triangle,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a triangle entity.
     *
     * @Route("/{id}", name="triangle_show")
     * @Method("GET")
     */
    public function showAction(Triangle $triangle)
    {
        $deleteForm = $this->createDeleteForm($triangle);

        return $this->render('triangle/show.html.twig', array(
            'triangle' => $triangle,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing triangle entity.
     *
     * @Route("/{id}/edit", name="triangle_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Triangle $triangle)
    {
        $deleteForm = $this->createDeleteForm($triangle);
        $editForm = $this->createForm('AppBundle\Form\TriangleType', $triangle);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('triangle_edit', array('id' => $triangle->getId()));
        }

        return $this->render('triangle/edit.html.twig', array(
            'triangle' => $triangle,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a triangle entity.
     *
     * @Route("/{id}", name="triangle_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Triangle $triangle)
    {
        $form = $this->createDeleteForm($triangle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($triangle);
            $em->flush($triangle);
        }

        return $this->redirectToRoute('triangle_index');
    }

    /**
     * Creates a form to delete a triangle entity.
     *
     * @param Triangle $triangle The triangle entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Triangle $triangle)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('triangle_delete', array('id' => $triangle->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
