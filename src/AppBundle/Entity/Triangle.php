<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Triangle
 *
 * @ORM\Table(name="triangle")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TriangleRepository")
 */
class Triangle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Point1X", type="string", length=255)
     */
    private $point1X;

    /**
     * @var string
     *
     * @ORM\Column(name="Point1Y", type="string", length=255)
     */
    private $point1Y;

    /**
     * @var string
     *
     * @ORM\Column(name="Point2X", type="string", length=255)
     */
    private $point2X;

    /**
     * @var string
     *
     * @ORM\Column(name="Point2Y", type="string", length=255)
     */
    private $point2Y;

    /**
     * @var string
     *
     * @ORM\Column(name="Point3X", type="string", length=255)
     */
    private $point3X;

    /**
     * @var string
     *
     * @ORM\Column(name="Point3Y", type="string", length=255)
     */
    private $point3Y;

    /**
     * @var string
     *
     * @ORM\Column(name="Couleur", type="string", length=255)
     */
    private $couleur;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Triangle
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set point1X
     *
     * @param string $point1X
     *
     * @return Triangle
     */
    public function setPoint1X($point1X)
    {
        $this->point1X = $point1X;

        return $this;
    }

    /**
     * Get point1X
     *
     * @return string
     */
    public function getPoint1X()
    {
        return $this->point1X;
    }

    /**
     * Set point1Y
     *
     * @param string $point1Y
     *
     * @return Triangle
     */
    public function setPoint1Y($point1Y)
    {
        $this->point1Y = $point1Y;

        return $this;
    }

    /**
     * Get point1Y
     *
     * @return string
     */
    public function getPoint1Y()
    {
        return $this->point1Y;
    }

    /**
     * Set point2X
     *
     * @param string $point2X
     *
     * @return Triangle
     */
    public function setPoint2X($point2X)
    {
        $this->point2X = $point2X;

        return $this;
    }

    /**
     * Get point2X
     *
     * @return string
     */
    public function getPoint2X()
    {
        return $this->point2X;
    }

    /**
     * Set point2Y
     *
     * @param string $point2Y
     *
     * @return Triangle
     */
    public function setPoint2Y($point2Y)
    {
        $this->point2Y = $point2Y;

        return $this;
    }

    /**
     * Get point2Y
     *
     * @return string
     */
    public function getPoint2Y()
    {
        return $this->point2Y;
    }

    /**
     * Set point3X
     *
     * @param string $point3X
     *
     * @return Triangle
     */
    public function setPoint3X($point3X)
    {
        $this->point3X = $point3X;

        return $this;
    }

    /**
     * Get point3X
     *
     * @return string
     */
    public function getPoint3X()
    {
        return $this->point3X;
    }

    /**
     * Set point3Y
     *
     * @param string $point3Y
     *
     * @return Triangle
     */
    public function setPoint3Y($point3Y)
    {
        $this->point3Y = $point3Y;

        return $this;
    }

    /**
     * Get point3Y
     *
     * @return string
     */
    public function getPoint3Y()
    {
        return $this->point3Y;
    }

    /**
     * Set couleur
     *
     * @param string $couleur
     *
     * @return Triangle
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get couleur
     *
     * @return string
     */
    public function getCouleur()
    {
        return $this->couleur;
    }
}

